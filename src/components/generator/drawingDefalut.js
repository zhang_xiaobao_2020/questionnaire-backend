export default [
	{
    "__config__": {
      "label": "问卷头部",
      "tag": "csmwj-header",
      "tagIcon": "radio",
      "layout": "colFormItem",
      "csmwjheader": true,
      "csmwjtext": true,
      "span": 24,
      "formId": 101,
      "renderKey": "1011615016641970"
    },
    "__slot__": {},
    "csmwjheader":{
      "value":'问卷标题'
    },
    "csmwjtext":{
      "value1":'感谢您能抽出几分钟时间来参加本次答题，现在我们就马上开始吧！'
    },
    "__vModel__": "field101"
  }
  /*, 
  {
    "__config__": {
      "label": "单选题",
      "tag": "csmwj-radio-group",
      "tagIcon": "radio",
      "layout": "colFormItem",
      "csmwj": "item",
      "csmwjrequired": true,
      "csmwjanswerscore": true,
      "csmoptions": true,
      "span": 24,
      "formId": 104,
      "renderKey": "1041614607727066"
    },
    "__slot__": {
      "options": [{
        "value": "选项一"
      }, {
        "value": "选项二"
      }, {
        "value": "选项三"
      }]
    },
    "csmwjtitle": "请选择一个选项",
    "__vModel__": "field104"
  }, 
  {
    "__config__": {
      "label": "问卷底部",
      "tag": "csmwj-footer",
      "tagIcon": "radio",
      "layout": "colFormItem",
      "csmwj": "footer",
      "csmwjrequired": false,
      "csmwjanswerscore": false,
      "csmoptions": false,
      "span": 24,
      "formId": 102,
      "renderKey": "1021614607719535"
    },
    "__slot__": {},
    "csmwjtext": "您已完成本次问卷，感谢您的帮助与支持",
    "__vModel__": "field102"
  }*/
]
