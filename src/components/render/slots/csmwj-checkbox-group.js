export default {
  options(h, conf, key) {
    const list = []
    conf.__slot__.options.forEach(item => {
      if(conf.csmwjshowscore!=null && conf.csmwjshowscore.method=="everyscore" && conf.csmwjshowscore.showscore===true){
        list.push(<el-checkbox label={item.value} border style="display: block;width:100%;margin-left: 0px;margin-top:10px">{item.value} ( {item.score} 分) </el-checkbox>);
      }else{
        list.push(<el-checkbox label={item.value} border style="display: block;width:100%;margin-left: 0px;margin-top:10px">{item.value}</el-checkbox>);
      }
    })

    return list
  }
}
