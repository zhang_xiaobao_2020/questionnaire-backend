import Vue from 'vue'
import { loadScriptQueue } from '@/utils/loadScript'
import axios from 'axios'
import Tinymce from '@/components/tinymce/index.vue'
import Csmlabel from '@/components/csmlabel/index.vue'
import CsmwjHeader from '@/components/csmwj-header/index.vue'
import CsmwjFooter from '@/components/csmwj-footer/index.vue'
import CsmwjGroupLine from '@/components/csmwj-group-line/index.vue'
import CsmwjScoring from '@/components/csmwj-scoring/index.vue'

import CsmwjRadioGroup from '@/components/csmwj-radio-group/index.vue'
import CsmwjCheckboxGroup from '@/components/csmwj-checkbox-group/index.vue'
import Csmwjscore from '@/components/csmwj-score/index.vue'
import Csmwjinput from '@/components/csmwj-input/index.vue'

import CsmwjinputScore from '@/components/csmwj-input-score/index.vue'
import CsmwjinputFixedScore from '@/components/csmwj-input-fixed-score/index.vue'


Vue.component('tinymce', Tinymce)
Vue.component('csmlabel', Csmlabel)
Vue.component('csmwj-header', CsmwjHeader)
Vue.component('csmwj-footer', CsmwjFooter)
Vue.component('csmwj-group-line', CsmwjGroupLine)
Vue.component('csmwj-scoring', CsmwjScoring)

Vue.component('csmwj-radio-group', CsmwjRadioGroup)
Vue.component('csmwj-checkbox-group', CsmwjCheckboxGroup)
Vue.component('csmwj-score', Csmwjscore)
Vue.component('csmwj-input', Csmwjinput)

Vue.component('csmwj-input-score', CsmwjinputScore); // 输入型分数题
Vue.component('csmwj-input-fixed-score', CsmwjinputFixedScore); // 固定分数型数据


Vue.prototype.$axios = axios

const $previewApp = document.getElementById('previewApp')
const childAttrs = {
  file: '',
  dialog: ' width="600px" class="dialog-width" v-if="visible" :visible.sync="visible" :modal-append-to-body="false" '
}

window.addEventListener('message', init, false)

function buildLinks(links) {
  let strs = ''
  links.forEach(url => {
    strs += `<link href="${url}" rel="stylesheet">`
  })
  return strs
}

function init(event) {
  if (event.data.type === 'refreshFrame') {
    const code = event.data.data
    const attrs = childAttrs[code.generateConf.type]
    let links = ''

    if (Array.isArray(code.links) && code.links.length > 0) {
      links = buildLinks(code.links)
    }

    $previewApp.innerHTML = `${links}<style>${code.css}</style><div id="app"></div>`

    if (Array.isArray(code.scripts) && code.scripts.length > 0) {
      loadScriptQueue(code.scripts, () => {
        newVue(attrs, code.js, code.html)
      })
    } else {
      newVue(attrs, code.js, code.html)
    }
  }
}

function newVue(attrs, main, html) {
  main = eval(`(${main})`)
  main.template = `<div>${html}</div>`
  new Vue({
    components: {
      child: main
    },
    data() {
      return {
        visible: true
      }
    },
    template: `<div><child ${attrs}/></div>`
  }).$mount('#app')
}
