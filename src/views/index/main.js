import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import '@/styles/index.scss'
import '@/icons'
import axios from 'axios'
import Tinymce from '@/components/tinymce/index.vue'
import Csmlabel from '@/components/csmlabel/index.vue'
import CsmwjHeader from '@/components/csmwj-header/index.vue'
import CsmwjFooter from '@/components/csmwj-footer/index.vue'
import CsmwjGroupLine from '@/components/csmwj-group-line/index.vue'
import CsmwjScoring from '@/components/csmwj-scoring/index.vue'

import CsmwjRadioGroup from '@/components/csmwj-radio-group/index.vue'
import CsmwjCheckboxGroup from '@/components/csmwj-checkbox-group/index.vue'
import Csmwjscore from '@/components/csmwj-score/index.vue'
import Csmwjinput from '@/components/csmwj-input/index.vue'

import CsmwjinputScore from '@/components/csmwj-input-score/index.vue'
import CsmwjinputFixedScore from '@/components/csmwj-input-fixed-score/index.vue'

Vue.component('tinymce', Tinymce)
Vue.component('csmlabel', Csmlabel)
Vue.component('csmwj-header', CsmwjHeader)
Vue.component('csmwj-footer', CsmwjFooter)
Vue.component('csmwj-group-line', CsmwjGroupLine)
Vue.component('csmwj-scoring', CsmwjScoring)

Vue.component('csmwj-radio-group', CsmwjRadioGroup)
Vue.component('csmwj-checkbox-group', CsmwjCheckboxGroup)
Vue.component('csmwj-score', Csmwjscore)
Vue.component('csmwj-input', Csmwjinput)

Vue.component('csmwj-input-score', CsmwjinputScore); // 输入型分数题
Vue.component('csmwj-input-fixed-score', CsmwjinputFixedScore); // 固定分数型数据


Vue.config.productionTip = false
Vue.prototype.$axios = axios

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
